# Serverless SNS Topic - Sample Infrastructure

## Overview

This is a CloudFormation template and bash script to simplify the demonstration of the Serverless SNS topic project.  This creates SNS topics and cloudwatch alarms to generate sample data for our SLS SNS project.

## Usage

### Creating stack
```bash
$ aws cloudformation --region us-east-1 create-stack --stack-name sls-sns-sample-infra --template-body $(cat cf.json)
```

### Deleting stack
```bash
$ aws cloudformation --region us-east-1 delete-stack --stack-name sls-sns-sample-infra
```
